package com.tom.repositories;

import com.tom.domain.Category;
import com.tom.domain.Difficulty;
import com.tom.domain.Game;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class GameRepository {

    public Game findById(int id) {
        try (Connection connection = DatabaseUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from game as g inner join category as c on g.category_id = c.id inner join difficulty as d on g.difficulty_id = d.id where g.id = ? ");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            return Game.builder()
                    .id(resultSet.getInt("g.id"))
                    .gameName(resultSet.getString("game_name"))
                    .editor(resultSet.getString("editor"))
                    .author(resultSet.getString("author"))
                    .age(resultSet.getString("age"))
                    .yearEdition(resultSet.getInt("year_edition"))
                    .minPlayers(resultSet.getInt("min_players"))
                    .maxPlayers(resultSet.getInt("max_players"))
                    .category(new Category(resultSet.getInt("c.id"),resultSet.getString("category_name")))
                    .playDuration(resultSet.getString("play_duration"))
                    .difficulty(new Difficulty(resultSet.getInt("d.id"), resultSet.getString("difficulty_name")))
                    .price(resultSet.getDouble("price"))
                    .image(resultSet.getString("image"))
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public List<Game> findAllGames() {
        try (Connection connection = DatabaseUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select game_name from games.game");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            List<Game> games = new ArrayList<>();
            while(resultSet.next()){
                Game game = Game.builder().gameName(resultSet.getString("game_name"))
                        .build();
                games.add(game);
            }
            return games;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public Game findByName(String name){
        try (Connection connection = DatabaseUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from game as g inner join category as c on g.category_id = c.id inner join difficulty as d on g.difficulty_id = d.id where LOWER(game_name) LIKE (?)");
            String newString = "%" + name + "%";
            preparedStatement.setString(1, newString);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            return Game.builder()
                    .id(resultSet.getInt("g.id"))
                    .gameName(resultSet.getString("game_name"))
                    .editor(resultSet.getString("editor"))
                    .author(resultSet.getString("author"))
                    .age(resultSet.getString("age"))
                    .yearEdition(resultSet.getInt("year_edition"))
                    .minPlayers(resultSet.getInt("min_players"))
                    .maxPlayers(resultSet.getInt("max_players"))
                    .category(new Category(resultSet.getInt("c.id"),resultSet.getString("category_name")))
                    .playDuration(resultSet.getString("play_duration"))
                    .difficulty(new Difficulty(resultSet.getInt("d.id"), resultSet.getString("difficulty_name")))
                    .price(resultSet.getDouble("price"))
                    .image(resultSet.getString("image"))
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
