package com.tom.repositories;

import com.tom.domain.Borrow;
import com.tom.domain.Borrower;
import com.tom.domain.CompleteBorrow;
import com.tom.domain.Game;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BorrowerRepository {

    public Borrower findById(int id) {
        try (Connection connection = DatabaseUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from borrower where id = ? ");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            return Borrower.builder().borrowerName(resultSet.getString("borrower_name")).id(resultSet.getInt("id"))
                    .street(resultSet.getString("street")).houseNumber(resultSet.getString("house_number"))
                    .busNumber(resultSet.getString("bus_number")).postalCode(resultSet.getString("postcode"))
                    .city(resultSet.getString("city")).telephone(resultSet.getString("telephone"))
                    .email(resultSet.getString("email")).build();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<CompleteBorrow> getBorrows() {
        try (Connection connection = DatabaseUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from game as g inner join difficulty as d on g.difficulty_id = d.id inner join borrow as b on g.id = b.game_id inner join borrower as br on b.borrower_id = br.id  order by borrower_name, borrow_date");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            List<CompleteBorrow> borrows = new ArrayList<>();
            while (resultSet.next()) {
                CompleteBorrow completeBorrow = CompleteBorrow.builder()
                        .borrow(Borrow.builder()
                                .game(Game.builder().gameName(resultSet.getString("game_name")).build())
                                .borrowDate(resultSet.getDate("borrow_date")).returnDate(resultSet.getDate("return_date"))
                                .build())
                        .borrower(Borrower.builder().borrowerName(resultSet.getString("borrower_name"))
                                .build())
                        .build();
                borrows.add(completeBorrow);
            }
            return borrows;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

