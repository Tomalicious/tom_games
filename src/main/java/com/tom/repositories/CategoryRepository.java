package com.tom.repositories;

import com.tom.domain.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CategoryRepository {

    public Category findById(int id){
        try (Connection connection = DatabaseUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from category where id = ? ");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();
            return Category.builder().id(resultSet.getInt("id")).categoryName(resultSet.getString("category_name")).build();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
