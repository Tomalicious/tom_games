package com.tom;

import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;
import com.tom.commandpattern.CommandsEnum;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class GamesApp {
    private static Scanner scanner = new Scanner(System.in);
    private static List<CommandsEnum> commandsEnumList = Arrays.stream(CommandsEnum.values()).collect(Collectors.toList());

    public static void main(String[] args) {
        while (true) {
            showOptions();
            try {
                chooseOption().getCommand().execute();
                System.out.println(); //Just a new line
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void showOptions() {
        System.out.println("----List of options-----");
        commandsEnumList.forEach(commandsEnum -> System.out.printf("\t%s\n", commandsEnum));
        System.out.println("---------");
    }

    private static CommandsEnum chooseOption() throws Exception {
        System.out.print("Choose an option: ");
        int optionId = scanner.nextInt();
        return commandsEnumList.stream()
                .filter(commandsEnum -> commandsEnum.getOptionId() == optionId).findFirst()
                .orElseThrow(() -> new Exception("C-N-F!!! -->  Command not found !!!"));
    }
}

