package com.tom.commandpattern;

import com.tom.domain.Borrower;
import com.tom.services.BorrowerService;
import com.tom.services.Service;

public class ShowFirstBorrower implements Command{

    private Service<Borrower> borrowerService = new BorrowerService();

    @Override
    public void execute() {
        System.out.println(borrowerService.findById(1));
    }
}
