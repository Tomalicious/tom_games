package com.tom.commandpattern;

import com.tom.domain.Borrower;
import com.tom.domain.CompleteBorrow;
import com.tom.repositories.BorrowerRepository;
import com.tom.services.BorrowerService;

import java.util.List;

public class ListOutAllBorrows implements Command {

    private BorrowerService brwService = new BorrowerService();

    @Override
    public void execute() {
        List<CompleteBorrow> allBorrows = brwService.getBorrows();
        for (CompleteBorrow cb : allBorrows){
            System.out.println(cb.getBorrow().getGame().getGameName());
            System.out.println(cb.getBorrower().getBorrowerName());
            System.out.println(cb.getBorrow().getBorrowDate());
            System.out.println(cb.getBorrow().getReturnDate());
        }

    }
}
