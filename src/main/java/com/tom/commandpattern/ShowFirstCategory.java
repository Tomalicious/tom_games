package com.tom.commandpattern;

import com.tom.domain.Game;
import com.tom.repositories.GameRepository;
import com.tom.services.GameServiceImpl;
import com.tom.services.Service;

public class ShowFirstCategory implements Command{
    private Service<Game> gameService = new GameServiceImpl();
    @Override
    public void execute() {
        System.out.println(gameService.findById(5));
    }
}
