package com.tom.commandpattern;

import com.tom.domain.Game;
import com.tom.services.GameServiceImpl;
import com.tom.services.Service;

import java.util.List;

public class AllGames implements Command {

    private Service<Game> gameList = new GameServiceImpl();

    @Override
    public void execute() {
        List<Game> allGames = gameList.findAll();
        allGames.forEach(game -> System.out.println(game));


    }

}

