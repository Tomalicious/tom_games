package com.tom.commandpattern;

public interface Command {
    void execute();
}
