package com.tom.commandpattern;

import com.tom.domain.Game;
import com.tom.services.GameServiceImpl;
import com.tom.services.Service;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class ShowAndChoose implements Command {
        private Scanner scanner = new Scanner(System.in);
        private Service<Game> gameService = new GameServiceImpl();

        @Override
        public void execute() {
            List<Game> allGames = gameService.findAll();
            for (Game g : allGames){
                System.out.println(g);

            }
            
            findByName();
        }

    private void findByName() {
        System.out.println("choose a game by name: ");
        String name = scanner.next().toLowerCase(Locale.ROOT);
        Game game = gameService.findByName(name);
        System.out.println(game.getGameName() + " / " + game.getCategory().getCategoryName());
    }


}
