package com.tom.commandpattern;

import com.tom.domain.Game;
import com.tom.services.GameServiceImpl;
import com.tom.services.Service;

public class ShowFifthGame implements Command{
    private Service<Game> gameService = new GameServiceImpl();
    @Override
    public void execute() {
        Game game = gameService.findById(5);
        System.out.println(game.toString());
    }
}
