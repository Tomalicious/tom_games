package com.tom.commandpattern;



import com.tom.domain.Game;

import com.tom.services.GameServiceImpl;
import com.tom.services.Service;

import java.util.Scanner;

public class ChooseGame implements Command {
    private Service<Game> gameService = new GameServiceImpl();
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void execute() {
        System.out.println("choose a game by id: ");
        System.out.println(gameService.findById(scanner.nextInt()));

    }
}
