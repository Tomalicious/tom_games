package com.tom.commandpattern;

import com.tom.domain.Category;
import com.tom.services.CategoryService;
import com.tom.services.Service;

public class ShowFirstGameCategory implements Command {
    private Service<Category> categoryService = new CategoryService();
    @Override
    public void execute() {
        Category category = categoryService.findById(1);
        System.out.println(category.toString());
    }
}
