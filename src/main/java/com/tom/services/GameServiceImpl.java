package com.tom.services;


import com.tom.domain.CompleteBorrow;
import com.tom.domain.Game;
import com.tom.repositories.GameRepository;

import java.util.List;

public class GameServiceImpl implements Service<Game> {

    //TODO link GameRepo to this class
    private GameRepository gameRepository = new GameRepository();

    @Override
    public Game findById(int id) {
        return gameRepository.findById(id); //TODO use the findById in the GameRepo
    }

    @Override
    public List<Game> findAll() {
        return gameRepository.findAllGames();
    }

    @Override
    public Game findByName(String name) {
        return gameRepository.findByName(name);
    }

    @Override
    public List<CompleteBorrow> getBorrows() {
        return null;
    }


}

