package com.tom.services;

import com.tom.domain.CompleteBorrow;

import java.util.List;

public interface Service <T>{
    T findById(int id);

    List<T> findAll();

    T findByName(String name);

    List<CompleteBorrow> getBorrows();
}