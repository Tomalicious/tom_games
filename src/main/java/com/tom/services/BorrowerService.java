package com.tom.services;

import com.tom.domain.Borrower;
import com.tom.domain.CompleteBorrow;
import com.tom.repositories.BorrowerRepository;

import java.util.List;

public class BorrowerService implements Service<Borrower>{

    private BorrowerRepository borrowerRepository = new BorrowerRepository();

    @Override
    public Borrower findById(int id) {
        return borrowerRepository.findById(id);
    }


    @Override
    public List<Borrower> findAll() {
        return null;
    }

    @Override
    public Borrower findByName(String name) {
        return null;
    }

    @Override
    public List<CompleteBorrow> getBorrows() {
        return borrowerRepository.getBorrows();
    }
}
