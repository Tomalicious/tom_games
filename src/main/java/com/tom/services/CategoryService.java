package com.tom.services;


import com.tom.domain.Category;
import com.tom.domain.CompleteBorrow;
import com.tom.repositories.CategoryRepository;

import java.util.List;

public class CategoryService implements Service<Category> {

    CategoryRepository categoryRepository = new CategoryRepository();

    @Override
    public Category findById(int id) {
        //TODO use categoryRepository
        return  categoryRepository.findById(id);
    }

    @Override
    public List<Category> findAll() {
        return null;
    }

    @Override
    public Category findByName(String name) {
        return null;
    }

    @Override
    public List<CompleteBorrow> getBorrows() {
        return null;
    }

}
