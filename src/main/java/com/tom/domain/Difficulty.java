package com.tom.domain;


import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
public class Difficulty extends BaseEntity{
    private String difficultyName;

    public Difficulty(int id, String difficultyName) {
        super(id);
        this.difficultyName = difficultyName;
    }
}
